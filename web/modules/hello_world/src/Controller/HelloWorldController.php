<?php

namespace Drupal\hello_world\Controller;

/**
 * Controller for the hello menu.
 */
class HelloWorldController {

  /**
   * Returns the output for the.
   */
  public function hello() {
    $first = 'first';
    $second = 'second';
    // Returns the title and markup.
    return [
      '#title' => 'Hello everyone!',
      '#markup' => 'Google is the most useful search engine tool.',
    ];
  }

}
